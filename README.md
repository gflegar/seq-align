seq-align
=========

This is a divide-and-conquer algorithm (O(n^2) time and O(n) memory complexity) for the sequence alignment problem.
It was created as a seminar for the course _Algorithm design and analysis_ held at University of Zagreb / Department of Mathematics.