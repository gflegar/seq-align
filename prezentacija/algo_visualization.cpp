#include <cstdio>
#include <string>
#include <algorithm>

using namespace std;

const int n = 7, m = 7;

void draw(const char *s, const char *r, int w[][m], int ci, int cj) {
	if (ci > 0 && cj > 0) printf("\\begin{frame}{Primjer}"
		   "\\begin{align*}o(%d,%d) &= \\min \\lbrace \\textcolor{red}{o(%d,%d)} + \\delta,"
				"\\textcolor{green}{o(%d,%d)} + \\delta, \\textcolor{blue}{o(%d,%d)}"
				" + \\textcolor{yellow}{\\alpha_{%c%c}} \\rbrace \\\\ &= "
				"\\min \\lbrace \\textcolor{red}{%d} + 1,"
				"\\textcolor{green}{%d} + 1, \\textcolor{blue}{%d}"
				" + \\textcolor{yellow}{%d} \\rbrace = %d\\end{align*}"
		   "\\begin{center}\\begin{tabular}{c | c |",
		    ci, cj, ci, cj - 1, ci - 1, cj, ci-1, cj - 1, s[ci], r[cj],
		    w[ci][cj-1], w[ci-1][cj], w[ci-1][cj-1], s[ci] != r[cj], w[ci][cj]);
	else printf("\\begin{frame}{Primjer}\\begin{align*} o(i,0) &= i \\delta = i \\\\ o(0, j) &= j \\delta = j \\end{align*}"
				"\\begin{center}\\begin{tabular}{c | c |");
	for (int j = 0; j < m; ++j) printf(" c");
	printf("} & ");
	for (int j = 0; j < m; ++j) printf("&\\circled{%d}{none}", j);
	printf("\\\\ \\hline & ");
	for (int j = 0; j < m; ++j) {
		if (j == cj) printf("&\\circled{%c}{yellow}", r[j]);
		else printf("&\\circled{%c}{none}", r[j]);
	}
	printf("\\\\ \\hline ");
	for (int i = 0; i < n; ++i) {
		if (i == ci) printf("%d&\\circled{%c}{yellow}", i, s[i]);
		else printf("%d&\\circled{%c}{none}", i, s[i]);
		for (int j = 0; j < m; ++j) {
			if (i == ci && j == cj) printf("&\\circled{%d}{black}", w[i][j]);
			else if (i == ci && j == cj - 1) printf("&\\circled{%d}{red}", w[i][j]);
			else if (i == ci - 1 && j == cj) printf("&\\circled{%d}{green}", w[i][j]);
			else if (i == ci - 1 && j == cj - 1) printf("&\\circled{%d}{blue}", w[i][j]);
			else if (w[i][j] == -1) printf("&\\circled{?}{none}");
			else printf("&\\circled{%d}{none}", w[i][j]);
		}
		printf("\\\\");
	}
	printf("\\end{tabular}\\end{center}\\end{frame}\n");
}

void draw_return(const char *s, const char *r, int w[][m], int ci, int cj, int *ps, int *pr, int pn, bool inr[][m]) {
	printf("\\begin{frame}{Primjer}\\begin{equation*} P = \\lbrace");
	for (int i = pn - 1; i >= 0; --i) {
		printf("(%d, %d)", ps[i], pr[i]);
		if (i > 0) printf(",");
	}
	if (ci > 0 && cj > 0) printf("\\rbrace \\end{equation*} \\begin{equation*} %d = \\min \\lbrace "
				"\\circled{\\textcolor{red}{%d}}{%s}, \\circled{\\textcolor{green}{%d}}{%s}, \\circled{\\textcolor{blue}{%d}}{%s} \\rbrace \\end{equation*}"
			"\\begin{center}\\begin{tabular}{c | c |",
			w[ci][cj], w[ci][cj - 1] + 1, (w[ci][cj - 1] + 1 == w[ci][cj]) ? "red" : "none",
			w[ci-1][cj] + 1, (w[ci-1][cj] + 1 == w[ci][cj]) ? "green" : "none",
			w[ci-1][cj-1] + (s[ci] != r[cj]), (w[ci-1][cj-1] + (s[ci] != r[cj]) == w[ci][cj]) ? "blue" : "none");
	else printf("\\rbrace \\end{equation*} \\begin{equation*} \\end{equation*}"
			"\\begin{center}\\begin{tabular}{c | c |");
	for (int j = 0; j < m; ++j) printf(" c");
	printf("} & ");
	for (int j = 0; j < m; ++j) printf("&\\circled{%d}{none}", j);
	printf("\\\\ \\hline & ");
	for (int j = 0; j < m; ++j) printf("&\\circled{%c}{none}", r[j]);
	printf("\\\\ \\hline ");
	for (int i = 0; i < n; ++i) {
		printf("%d&\\circled{%c}{none}", i, s[i]);
		for (int j = 0; j < m; ++j) {
			if (i == ci && j == cj) printf("&\\circled{%d}{black}", w[i][j]);
			else if (i == ci && j == cj - 1 && ci > 0) printf("&\\circled{%d}{red}", w[i][j]);
			else if (i == ci - 1 && j == cj && cj > 0) printf("&\\circled{%d}{green}", w[i][j]);
			else if (i == ci - 1 && j == cj - 1) printf("&\\circled{%d}{blue}", w[i][j]);
			else if (inr[i][j]) printf("&\\circled{%d}{yellow}", w[i][j]);
			else printf("&\\circled{%d}{none}", w[i][j]);
		}
		printf("\\\\");
	}
	printf("\\end{tabular}\\end{center}\\end{frame}\n");
}

int main(void) {
	char *s = " nizovi", *r = " izkvui";
	int ps[50], pr[50], pn;
	int w[n][m];
	bool inr[n][m];
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			if (i == 0) w[i][j] = j;
			else if (j == 0) w[i][j] = i;
			else w[i][j] = -1;
			inr[i][j] = false;
		}
	}
	draw(s, r, w, -1, -1);
	for (int i = 1; i < n; ++i) {
		for (int j = 1; j < m; ++j) {
			w[i][j] = min(min(w[i-1][j], w[i][j-1]) + 1, w[i-1][j-1] + (s[i] != r[j]));
			draw(s, r, w, i, j);
		}

	}

	int ci = n - 1, cj = m -1;
	inr[ci][cj] = true;
	draw_return(s, r, w, ci, cj, ps, pr, 0, inr);
	while (ci != 0 && cj != 0) {
		for (int k = 3; k > 0; --k) {
			if (w[ci][cj] == w[ci-(k>>1)][cj-(k&1)] + ((k == 3) ? (s[ci] != r[cj]) : 1)) {
				if (k == 3) {
					ps[pn] = ci;
					pr[pn++] = cj;
				}
				inr[ci][cj] = true;
				ci -= (k>>1);
				cj -= (k&1);
				break;
			}
		}
		draw_return(s, r, w, ci, cj, ps, pr, pn, inr);
	}
	return 0;
}
