#include "SeqAlign.h"

inline int min(int a, int b) {
    return (a < b) ? a : b;
}

int alignSequences(char *s, int n, char *r, int m, int *w, char *p, int *pn,
                   int t[][128], int d) {
    int i, j, c, o = m + 1, *wp;
    w[0] = 0;
    for (wp = w + 1; wp < w + o; ++wp) *wp = *(wp-1) + d;
    for (i = 0; i < n; ++i) {
        *wp = *(wp-o) + d;
        ++wp;
        for (j = 0; j < m; ++j, ++wp) {
            *wp = min(*(wp-1), *(wp-o)) + d;
            *wp = min(*wp, *(wp-o-1) + t[s[i]][r[j]]);
        }
    }

    *pn=0;
    while (i != 0 || j != 0) {
        for (c = 3; c > 0; --c) {
            if (i - (c>>1) < 0 || j - (c&1) < 0) continue;
            if (w[i*o + j] == w[(i-(c>>1))*o + j-(c&1)]
                                + (c == 3 ? t[s[i-1]][r[j-1]] : d))
                break;
        }
        p[(*pn)++] = c;
        i -= (c>>1);
        j -= (c&1);
    }
    return w[n*o + m];
}

int alignSequencesM(char *s, int n, char *r, int m, int *w, int t[][128], int d) {
    int i, j, o = m + 1;
    w[0] = 0;
    for (j = 0; j < m; ++j) w[j+1] = w[j] + d;
    for (i = 0; i < n; ++i) {
        w[(i+1&1)*o] = w[(i&1)*o] + d;
        for (j = 0; j < m; ++j) {
            w[(i+1&1)*o + j+1] = min(w[(i+1&1)*o + j], w[(i&1)*o + j+1]) + d;
            w[(i+1&1)*o + j+1] = min(w[(i+1&1)*o + j+1], w[(i&1)*o + j] + t[s[i]][r[j]]);
        }
    }
    return w[(n&1)*o + m];
}

int alignSequencesReversedM(char *s, int n, char *r, int m, int *w, int t[][128], int d) {
    int i, j, o = m + 1;
    w[(n&1)*o + m] = 0;
    for (j = m; j > 0; --j) w[(n&1)*o + j-1] = w[(n&1)*o + j] + d;
    for (i = n; i > 0; --i) {
        w[(i+1&1)*o + m] = w[(i&1)*o + m] + d;
        for (j = m; j > 0; --j) {
            w[(i+1&1)*o + j-1] = min(w[(i+1&1)*o + j], w[(i&1)*o + j-1]) + d;
            w[(i+1&1)*o + j-1] = min(w[(i+1&1)*o + j-1], w[(i&1)*o + j] + t[s[i-1]][r[j-1]]);
        }
    }
    return w[0];
}


int alignSequencesDC(char *s, int n, char *r, int m, int *w, char *p, int *pn,
                     int t[][128], int d) {
    int q = -1, l = 0, i, tmp;
    if (n <= 2 || m <= 2)
        return alignSequences(s, n, r, m, w, p, pn, t, d);
    alignSequencesReversedM(s + n/2, n - n/2, r, m, w, t, d);
    alignSequencesM(s, n/2, r, m, w + m + 1, t, d);
    for (i = 0; i <=m; ++i)
        if ((tmp = w[i] + w[((n/2&1)+1)*(m + 1) + i]) < l || q == -1) {
            l = tmp;
            q = i;
        }
    alignSequencesDC(s + n/2, n - n/2, r + q, m - q, w, p, pn, t, d);
    alignSequencesDC(s, n/2, r, q, w, p + *pn, &tmp, t, d);
    *pn += tmp;
    return l;
}

void visualizeAlignment(char *s, int n, char *r, int m, char *p, int pn) {
    int i;
    s[pn] = r[pn] = '\0';
    for (i = 0; i < pn; ++i) {
        s[pn-i-1] = (p[i]>>1) ? s[--n] : '-';
        r[pn-i-1] = (p[i]&1) ? r[--m] : '-';
    }
}
