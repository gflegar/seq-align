#ifndef SEQALIGN_H
#define SEQALIGN_H

int alignSequences(char *s, int n, char *r, int m, int *w, char *p, int *pn,
                   int t[][128], int d);

int alignSequencesM(char *s, int n, char *r, int m, int *w, int t[][128], int d);

int alignSequencesReversedM(char *s, int n, char *r, int m, int *w, int t[][128], int d);

int alignSequencesDC(char *s, int n, char *r, int m, int *w, char *p, int *pn,
                     int t[][128], int d);
void visualizeAlignment(char *s, int n, char *r, int m, char *p, int pn);

#endif // SEQALIGN_H

