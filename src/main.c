#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "SeqAlign.h"

void genSimpleTable(int t[][128]) {
    int i,j;
    for (i = 0; i < 128; ++i)
        for (j = 0; j < 128; ++j)
            t[i][j] = (i != j);
}

void genChars(char *s, int size) {
    int i;
    for (i = 0; i < size; ++i) {
        s[i] = 'a' + rand() % ('z' - 'a' + 1);
    }
}

double getTime(void) {
    return (double)clock() / CLOCKS_PER_SEC;
}

inline int max(int n, int m) {
    return (n < m) ?  m : n;
}

void test(int iters, int minn, int maxn, int step, int diff, FILE *out, FILE *log) {
    int size, tests, n, m, i, d = 1;
    int *work = NULL;
    int t[128][128];
    int checksum, tmp;
    char *s = NULL, *p = NULL;
    double t1, t2, t3;
    genSimpleTable(t);
    fprintf(out, "   N*M   \t   N  \t   M  \t    align   \t   alignDC  \t       align/NM       \t      alignDC/NM      \n");
    for (n = minn; n <= maxn; n += step) {
        m = n / diff;
        fprintf(log, "Testing on problem size %d x %d = %d\n", n, m, n * m);
        tests = iters / (n * m);
        if (tests == 0) ++tests;
        fprintf(log, "Tests to do: %d\n", tests);
        s = (char*)realloc(s, max(n, m) * tests * sizeof(char));
        p = (char*)realloc(p, (n + m) * sizeof(char));
        work = (int*)realloc(work, max((n + 1) * (m + 1), 3 * max(n, m) + 3) * sizeof(int));

        if (s == NULL || p == NULL || work == NULL) {
            fprintf(log, "Unable to allocate memory\n");
            return;
        }
        genChars(s, max(n, m) * tests);
        checksum = 0;

        fprintf(log, "Running standard algorithm...");
        t1 = getTime();
        for (i = 0; i < tests; ++i) {
            fprintf(log, "\rRunning standard algorithm...%d", i + 1);
            fflush(log);
            checksum ^= alignSequences(s + i * n, n, s + ((i + 1) % tests) * n, m, work, p, &tmp, t, d);
        }
        t2 = getTime() - t1;
        fprintf(log, "\nTotal time: %lf; Average time: %lf\n"
                     "Running memory efficient algorithm...", t2, t2 / tests);
        t1 = getTime();
        for (i = 0; i < tests; ++i) {
            fprintf(log, "\rRunning memory efficient algorithm...%d", i + 1);
            checksum ^= alignSequencesDC(s + i * n, n, s + ((i + 1) % tests) * n, m, work, p, &tmp, t, d);
        }
        t3 = getTime() - t1;
        fprintf(log, "\nTotal time: %lf; Average time: %lf\n", t3, t3 / tests);

        fprintf(out, "%10d\t%6d\t%6d\t%.10lf\t%.10lf\t%.20lf\t%.20lf\n", n * m, n, m,
                t2 / tests, t3 / tests,
                t2 / tests / n / m, t3 / tests /n / m);
        if (checksum) {
            fprintf(log, "Something went wrong, checksum = %d", checksum);
            return;
        }
    }
    free(s);
    free(p);
    free(work);
}

int main(int argc, char *argv[]) {
    /*int t[128][128], d = 1;
    int n, m, np, i;
    char s[201], r[201], p[400];
    int w[40401];*/
    srand(time(NULL));

    test(100000000, 500, 10000, 500, 3, stdout, stderr);
    /*genSimpleTable(t);
    scanf("%s%s", s, r);

    n = strlen(s);
    m = strlen(r);



    printf("%d = %d = %d = %d\n",
           alignSequences(s, n, r, m, w, p, &np, t, d),
           alignSequencesM(s, n, r, m, w, t, d),
           alignSequencesReversedM(s, n, r, m, w, t, d),
           alignSequencesDC(s, n, r, m, w, p, &np, t, d));

    visualizeAlignment(s, n, r, m, p, np);
    printf("\n%s\n%s\n", s, r);*/
    return 0;
}
