TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    src/main.c \
    src/SeqAlign.c

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    src/SeqAlign.h

